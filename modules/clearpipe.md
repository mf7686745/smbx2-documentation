# Clear pipes

Clear pipes automatically register a selection of NPCs
as candidates for passing through. You can modify these lists as
follows:

```lua
local clearpipe = require("blocks/ai/clearpipe")
--Register Goomba and unregister player fireball
clearpipe.registerNPC(1)
clearpipe.unregisterNPC(13)
```